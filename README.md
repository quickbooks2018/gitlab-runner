# cloudgeeks gitlab runner

- cloudgeeks.ca


- Gitlab runner configuration
- https://docs.gitlab.com/runner/register/index.html#docker

```configuration
docker run --rm -it -e privileged=true -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```

- Gitlab runner
- https://docs.gitlab.com/runner/install/docker.html

```gitlab-runner
docker run -id --name gitlab-runner --restart unless-stopped -v $(which docker):/usr/bin/docker -v /var/run/docker.sock:/var/run/docker.sock -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest
```

```restart
docker exec -it gitlab-runner bash
cd /etc/gitlab-runner
apt update -y && apt install -y vim nano
vim config.toml
---> changed priviled ---> to true

docker restart gitlab-runner
```

- gitlab cicd setup

- .gitlab-ci.yml

```gitlab-ci.yml
image: python:latest

before_script:
# Install
  - apt update -y
  - apt install -y jq

build:
  script:
    - docker-compose --project-name app up -d --build
```
- .gitlab-ci.yml
```gitlab-ci.yaml
image: docker:19.03.1

variables:
    DOCKERFILE_LOCATION: ./
    DOCKER_VERSION: Docker-19.03.6-ce
    DOCKER_TLS_CERTDIR: ""

services:
  - docker:19.03.1-dind

stages:
  - build
  - deploy


###############
#   Builds    #
###############

build-docker-latest:
  stage: build
  environment:
    name: dev
  script:
    - echo "AWSCLI Installation"
    - docker ps -a
    - docker-compose --version

  only:
    - main
   

###############
#   Deploy    #
###############

deploy-to-application-repo:
  stage: deploy
  environment:
    name: dev
  script:
    - echo "hello"
  only:
    - main

```

- docker-compose installation

```
# https://docs.docker.com/compose/cli-command/
# https://docs.docker.com/compose/profiles/
# https://github.com/EugenMayer/docker-image-atlassian-jira/blob/master/docker-compose.yml

#########################################################################################
# 1 Run the following command to download the current stable release of Docker Compose
#########################################################################################

 mkdir -p ~/.docker/cli-plugins/
 curl -SL https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
 
 ###############################################
 # 2 Apply executable permissions to the binary
 ###############################################
 
  chmod +x ~/.docker/cli-plugins/docker-compose
  
  ###############################################
  # 3 Apply executable permissions to the binary
  ###############################################
  
  docker compose version
  
  
  
  
  # Commands
  # Build a Specific Profile
 #  docker compose -p app up -d --build

###########################
# Docker Compose Version 1
###########################
# https://docs.docker.com/compose/install/

curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version
```

- docker-compose commands
```docker-compose

docker-compose up -d --build

docker-compose --project-name app up -d --build
```
